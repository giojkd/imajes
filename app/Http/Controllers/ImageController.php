<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Image;
use File;
use Storage;


class ImageController extends Controller
{
    //
    public static function accountAllowsHost($account,$host){


        $configFilePath = storage_path() . '/app/accountAllowedHosts.txt';

        if (!File::isFile($configFilePath)) {
            return true;
        }
        $rows = collect(array_filter(preg_split('/\r\n|[\r\n]/', Storage::disk('local')->get('accountAllowedHosts.txt'))));

        $check = [];
        $accounts = [];
        if(count($rows) > 0){
            foreach($rows as $row){
                $items = array_filter(explode(' ', $row));
                $index = $items[0];
                $accounts[] = $index;
                unset($items[0]);
                $check[$index] = array_values($items);
            }
        }


        if (!in_array($account, $accounts)) {
            abort(403,'account does not exist');
        }

        if(!in_array($host,$check[$account])){
            abort(403,'image host is not allowed');
        }

    }

    public function resize(Request $request,$account,$original){
        $sRGBProfile = '/var/www/imajes/sRGB-IEC61966-2.1.icc'; // Inserisci il percorso al file ICC di sRGB
        if (strpos($original, 'http') == false) {
            #return false;
        }
        $width = ($request->has('width')) ? $request->width : 0;
        $height = ($request->has('height')) ? $request->height : 0;
        $operation = ($request->has('operation')) ? $request->operation : 'resize';
        $format = ($request->has('force_format')) ? $request->force_format : 'jpeg';
        $invalidateCache = ($request->has('invalidate_cache')) ? $request->invalidate_cache : 0;
        $rotate = ($request->has('rotate')) ? $request->rotate : 0;
        $quality = ($request->has('q')) ? $request->q : 90;
        $icc = ($request->has('icc')) ? true : false;

        $originalHost = parse_url($original);

        self::accountAllowsHost($account, $originalHost['host']);

        $requestData = $request->all();

        if(isset($requestData['invalidate_cache'])){
            unset($requestData['invalidate_cache']);
        }

        $encryptedOriginal = md5($original . '?' . http_build_query($requestData)) . '.' . $format;

        $saveFolder = storage_path() . '/app/public/images/' . $account . '/';
        if (!File::isDirectory($saveFolder)) {
            File::makeDirectory($saveFolder, 0755, true, true);
        }
        $savePath = $saveFolder. $encryptedOriginal ;

        if (file_exists($savePath) && !$invalidateCache) {
            return response()->file($savePath);
        }

        $original = str_replace(' ', '%20', $original);

        $img = Image::make($original);

        //$img->icc($sRGBProfile);

        #IMG FIT BEGIN
        if ($operation == 'fit') {
            if ($width > 0 && $height == 0) {
                $img = $img->fit($width);
            }

            if ($height > 0 && $width > 0) {
                $img = $img->fit($width, $height);
            }
        }
        #IMG FIT END

        #IMG RESIZE BEGIN
        if ($operation == 'resize') {

            if ($width > 0 && $height == 0) {
                $img = $img->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            if ($height > 0 && $width == 0) {
                $img = $img->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            if ($height > 0 && $width > 0) {
                $img = $img->resize($width, $height);
            }
        }
        #IMG RESIZE END

        if($rotate != 0){
            $img->rotate($rotate);
        }

        $img = $img->encode($format, $quality);

        if ($icc) {
            $imgc = Image::make($img)->getCore();
            $icc_profile = file_get_contents($sRGBProfile);
            $imgc->profileImage('icc', null); // Usa null per rimuovere il profilo esistente
            $imgc->profileImage('icc', $icc_profile);
            $img = Image::make($imgc);
        }
        $img->save($savePath,$quality);
        return $img->response();
    }
}
